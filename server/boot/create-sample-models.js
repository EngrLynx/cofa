var async = require('async');

module.exports = function(app) {

  var budget = app.dataSources.budget;
  async.parallel({
    Customers: async.apply(createCustomers),
    Goals: async.apply(createGoals),
  }, function(err, results) {
    if (err) throw err;
  });

  function createCustomers(cb) {
    budget.automigrate('Customer', function(err) {
      if (err) return cb(err);
      var Customer = app.models.Customer;
      Customer.create([{
        email: 'full.stack.engr@lynx.rocks',
        password: 'notmyrealpassword'
      }], cb);
    });
  }

  function createGoals(cb) {
    budget.automigrate('Goal', function(err) {
      if (err) return cb(err);
      var Goal = app.models.Goal;
      Goal.create([{
        purpose: 'dummy',
        amount: 0,
        targetDate: '2016-11-27'
      }], cb);
    });
  }

};